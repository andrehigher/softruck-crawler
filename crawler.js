var Crawler 				= require( 'crawler' ),
	url 					= require( 'url' ),
	request 				= require( 'request' ),
	jsdom 					= require( 'jsdom' ),
	mongoose				= require( 'mongoose' ),
	fs 						= require('fs'),
	Anp 					= require( './anp' );

mongoose.connect('mongodb://localhost/softruck');

var Cod_Combustivel 		= '',
	selCombustivel 			= '',
	selSemana 				= '',
	desc_Semana 			= '',
	cod_Semana 				= '',
	tipo 					= '',
	selEstado 				= '',
	selNameEstado			= '',
	selMunicipio 			= '',
	cod_semana 				= '',
	desc_semana 			= '',
	cod_combustivel 		= '',
	desc_combustivel		= '',
	tipo 					= '';

var finalJSON = [];
 
var c = new Crawler({
    maxConnections : 1,

    // This will be called for each crawled page 
    callback : function ( error, result, $ ) {

    	// If loads the anp page properly 
        if( $( '#divQuadro' ).length ) {

        	var states =  $( 'select[name=selEstado] option');

        	for (var i = 0; i < states.length; i++) {
        		
        		
	        	desc_Semana = $( 'input[name=desc_Semana]' ).val();
	        	selNameEstado = $( states[ i ] ).text();
	        	selEstado = $( states[ i ] ).val();

	        	var temp = {
	        		name: selNameEstado,
	        		cities: []
	        	}
	        	finalJSON.push(temp);

	        	var dates 		= desc_Semana.split( 'a' ),
	        		startDate 	= dates[ 0 ].slice( 3 ).split( '/' ),
	        		endDate		= dates[ 1 ].split( '/' );


    			Cod_Combustivel = $('select[name=selCombustivel]').val();
	        	selCombustivel = $('select[name=selCombustivel]').val();
	        	selSemana = $('input[name=selSemana]').val();
	        	cod_Semana = $('input[name=cod_Semana]').val();
	        	tipo = $('input[name=tipo]').val();

	        	crawlCities(selNameEstado,Cod_Combustivel,selCombustivel,selSemana,desc_Semana,cod_Semana,tipo,selEstado);
        	}
        }
    }
});

function crawlCities( selNameEstado, Cod_Combustivel, selCombustivel, selSemana, desc_Semana, cod_Semana, tipo, selEstado ) {

	request.post(
	    'http://www.anp.gov.br/preco/prc/Resumo_Por_Estado_Municipio.asp', { 
	    	form: { 
	    		'Cod_Combustivel': Cod_Combustivel,
	    		'selCombustivel': selCombustivel,
	    		'selSemana': selSemana,
	    		'desc_Semana': desc_Semana,
	    		'cod_Semana': cod_Semana,
	    		'tipo': tipo,
	    		'selEstado': selEstado
	    	} 
	    },
	    function (error, response, body) {

	        if (!error && response.statusCode == 200) {
	            jsdom.env({
				    html: body,
				    scripts: [ 'http://code.jquery.com/jquery-1.5.min.js' ],
				    done: function (err, window) {

				    	console.log('**** COLLECTING ' + selNameEstado + '...' );
					    var $ = window.$;

					    $( '.table_padrao tr' ).each( function() {

					    	var averagePrice 			= 0,
					    		standardDeviation 		= 0,
					    		minPrice 				= 0,
					    		maxPrice 				= 0,
					    		averageMargin 			= 0,
					    		distAveragePrice		= 0,
					    		distStandardDeviation	= 0,	
					    		distMinPrice			= 0,
					    		distMaxPrice			= 0;


					    	if( $( this ).find( '.linkpadrao' ).length ) {
					    		var statistics = [];
					    		var nameCity = $( this ).find( 'td a' ).html();

					    		var dataCities = $( this ).find( 'td' );
					    		$.each( dataCities, function ( index, value ) {

					    			switch( index ) {
					    				case 2:
					    					averagePrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 3: 
					    					standardDeviation = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 4: 
					    					minPrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 5:
					    					maxPrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 6:
					    					averageMargin = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 7:
					    					distAveragePrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 8: 
					    					distStandardDeviation = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 9: 
					    					distMinPrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;

					    				case 10:
					    					distMaxPrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    				break;
					    			}
					    		})

					    		var tempStatistics = {
					    			type: selCombustivel.split( '*' )[ 1 ],
					    			consumerPrice: [{
					    				averagePrice:  averagePrice,
					    				standardDeviation: standardDeviation,
					    				minPrice: minPrice,
					    				maxPrice: maxPrice,
					    				averageMargin: averageMargin
					    			}],
					    			distributionPrice: [{
					    				averagePrice:  distAveragePrice,
					    				standardDeviation: distStandardDeviation,
					    				minPrice: distMinPrice,
					    				maxPrice: distMaxPrice,
					    			}]
					    		};
					    		statistics.push(tempStatistics);

						    	var linkSplited = $( this ).find( '.linkpadrao' ).attr( 'href' ).split( '(\'' );
					    		linkSplited = linkSplited[ 1 ].split( '\')' );

								var selMunicipio = linkSplited[ 0 ];
								var cod_semana = $( 'input[name=cod_semana]' ).val();
								var desc_semana = $( 'input[name=desc_semana]' ).val();
								var cod_combustivel = $( 'input[name=cod_combustivel]' ).val();
								var desc_combustivel = $( 'input[name=desc_combustivel]' ).val();
								var tipo = $( 'input[name=tipo]' ).val();

								crawlGasStations( selNameEstado, Cod_Combustivel, selCombustivel, selSemana, desc_Semana, cod_Semana, tipo, selEstado, nameCity, selMunicipio, cod_semana, desc_semana, cod_combustivel, desc_combustivel, tipo, statistics );
					    	}
					    });
				  	}
			  	});
			}
	    }
	);
}

function crawlGasStations( selNameEstado, Cod_Combustivel, selCombustivel, selSemana, desc_Semana, cod_Semana, tipo, selEstado, nameCity, selMunicipio, cod_semana, desc_semana, cod_combustivel, desc_combustivel, tipo, statistics ) {

	request.post(
	    'http://www.anp.gov.br/preco/prc/Resumo_Semanal_Posto.asp', { 
	    	form: { 
	    		'selMunicipio': selMunicipio,
	    		'selCombustivel': selCombustivel,
	    		'cod_semana': cod_semana,
	    		'desc_semana': desc_semana,
	    		'cod_combustivel': cod_combustivel,
	    		'desc_combustivel': desc_combustivel,
	    		'tipo': tipo
	    	} 
	    },
	    function (error, response, body) {

	        if (!error && response.statusCode == 200) {
	            jsdom.env({
				    html: body,
				    scripts: [ 'http://code.jquery.com/jquery-1.5.min.js' ],
				    done: function (err, window) {
				    	// console.log(finalJSON);
					    var $ = window.$;

					    var gasStations = [];
					    $( '#postos_nota_fiscal .table_padrao tr' ).each(function () {

					    	var name = '',
					    		address = '',
					    		area = '',
					    		flag = '',
					    		type = '',
					    		sellPrice = '',
					    		buyPrice = '',
					    		saleMode = '',
					    		provider = '',
					    		date = '';

					    	var dataGasStations = $( this ).find( 'td' );
				    		$.each( dataGasStations, function ( index, value ) {

					    		switch( index ) {

					    			case 0:
					    				name = $( value ).html();
					    			break;

					    			case 1:
					    				address = $( value ).html();
					    			break;

					    			case 2:
					    				area = $( value ).find( 'a' ).html();
					    			break;

					    			case 3:
					    				flag = $( value ).html();
					    			break;

					    			case 4:
					    				sellPrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    			break;

					    			case 5:
					    				buyPrice = parseFloat( $( value ).html().replace( ',', '.' ) );
					    			break;

					    			case 6:
					    				saleMode = $( value ).html();
					    			break;

					    			case 7:
					    				provider = $( value ).html();
					    			break;

					    			case 8:
					    				var flag = $( value ).html();
					    				flag = flag.split( '/' );
					    				date = new Date( flag[ 2 ].trim() + '-' + flag[ 1 ].trim() + '-' + flag[ 0 ].trim() );
					    			break;
					    		}
					    	});
					    		

				    		var tempGasStations = {
				    			name: name,
				    			address: address,
				    			area: area,
				    			flag: flag,
				    			prices: [{
				    				type: selCombustivel.split( '*' )[ 1 ],
				    				sellPrice: sellPrice,
				    				buyPrice: buyPrice,
				    				saleMode: saleMode,
				    				provider: provider,
				    				date: date
				    			}]
				    		};

				    		gasStations.push(tempGasStations);
					    });
					
				    	var tempCity = {
				    		name: nameCity,
				    		statistics: statistics,
				    		stations: gasStations
				    	};
				    	var cities = [];
				    	cities.push(tempCity);

				    	for (var i = 0; i < finalJSON.length; i++){
							if (finalJSON[i].name == selNameEstado){
								finalJSON[i].cities.push(cities);
							}
						}
						
						fs.writeFile("crawler.json", JSON.stringify(finalJSON, null, 2), function(err) {
						    if(err) {
						        return console.log(err);
						    }
						}); 
				  	}
			  	});
			}
	    }
	);
}
 
// // Queue just one URL, with default callback 
c.queue( 'http://www.anp.gov.br/preco/prc/Resumo_Por_Estado_Index.asp' );

